<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaires</title>
</head>
<body>

<!-- 
Exercice 5 Créer un formulaire sur la page index.php avec :

Une liste déroulante pour la civilité (Mr ou Mme)
Un champ texte pour le nom
Un champ texte pour le prénom

Ce formulaire doit rediriger vers la page index.php.
 Vous avez le choix de la méthod -->
<form method='GET' action="">
        <label for="civilité">Civilité:</label>
        <select id="civilité" name="civilité">
            <option value="mr">Mr</option>
            <option value="mme">Mme</option>
        </select>
        <br>
        <label for="fname">Nom:</label><br>
        <input type="text" name="nom3" required><br>
        <label for="lname">Prénom:</label><br>
        <input type="text" name="prénom3" required>
        <br>

<!--         
        <input type="submit" value="Submit"> -->

        <br>
        <br>
        <label for="filename">Nom du Fichier:</label><br>
        <input type="text" name="nomf" required><br>
        <label for="fileextension">Extension du fichier:</label><br>
        <select id="fileextension" name="fileextension">
            <!-- <option value="text">.text</option>
            <option value=".html">.html</option>
            <option value=".php">.php</option> -->
            <option value="pdf" selected>pdf</option>
            <!-- <option value="others">others</option> -->
        </select>
        <br>
        <input type="submit" value="Submit">
    </form>
    <!-- Exercice 6 Avec le formulaire de l'exercice 5, Si des données sont passées en POST ou en GET, le formulaire ne doit pas être affiché. Par contre les données transmises doivent l'être. Dans le cas contraire,
     c'est l'inverse. N'utiliser qu'une seule page. -->
    <?php
    $civilité = $_GET['civilité'];
    $name3 = $_GET['nom3'];
    $pname3 =  $_GET['prénom3'];
    echo "<h3> $civilité $name3  $pname3 </h3>";

    // Exercice 8 Sur le formulaire de l'exercice 6, en plus de ce qui est demandé sur les exercices précédent, vérifier que 
    // le fichier transmis est bien un fichier pdf.

    $nfichier = $_GET['nomf'];
    $extension = $_GET['fileextension'];
   
    echo "<h3> $nfichier.$extension </h3>";
    ?>
</body>
</html>